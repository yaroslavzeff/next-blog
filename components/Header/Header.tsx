import React from "react";
import Link from "next/link";
import styles from './styles.module.css'

const Header = () => {
  return (
    <header className={styles.header}>
      <div className={styles.wrapper}>
        <span><Link href="/">Home</Link></span>
        <span><Link href={{pathname: "/blog"}}>Blog</Link></span>
        <span><Link href={{pathname: "/about"}}>About</Link></span>
      </div>
    </header>
  );
};

export default Header;
