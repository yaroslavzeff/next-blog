import React from "react";
import styles from './styles.module.css'

const Blog = () => {
  return (
    <div className={styles.wrapper}>
      Blog about my life.
    </div>
  );
};

export default Blog;
