import React from "react";

type Props = {
  params: {
    id: string
  }
}

const Post = ({params: {id}}: Props) => {
  return (
    <div>
      <h2>Post page {id}</h2>
    </div>
  );
};

export default Post;
